using System;
using System.Security.Cryptography;
using System.Text;

namespace MyPosLibrary
{ 
    public class RSASignature
    {
        // Funci�n que se encarga de retornar la clave de acceso a la pasarela de pago.
        public string CreateSignature(string signatureString)
        {
            // Clave privada cedida por MyPOS necesaria para generar clave de acceso.
            string sPrivateKeyPEM = "-----BEGIN RSA PRIVATE KEY-----" +
                 Environment.NewLine + "MIICXAIBAAKBgQCf0TdcTuphb7X+Zwekt1XKEWZDczSGecfo6vQfqvraf5VPzcnJ" +
                 Environment.NewLine + "2Mc5J72HBm0u98EJHan+nle2WOZMVGItTa/2k1FRWwbt7iQ5dzDh5PEeZASg2UWe" +
                 Environment.NewLine + "hoR8L8MpNBqH6h7ZITwVTfRS4LsBvlEfT7Pzhm5YJKfM+CdzDM+L9WVEGwIDAQAB" +
                 Environment.NewLine + "AoGAYfKxwUtEbq8ulVrD3nnWhF+hk1k6KejdUq0dLYN29w8WjbCMKb9IaokmqWiQ" +
                 Environment.NewLine + "5iZGErYxh7G4BDP8AW/+M9HXM4oqm5SEkaxhbTlgks+E1s9dTpdFQvL76TvodqSy" +
                 Environment.NewLine + "l2E2BghVgLLgkdhRn9buaFzYta95JKfgyKGonNxsQA39PwECQQDKbG0Kp6KEkNgB" +
                 Environment.NewLine + "srCq3Cx2od5OfiPDG8g3RYZKx/O9dMy5CM160DwusVJpuywbpRhcWr3gkz0QgRMd" +
                 Environment.NewLine + "IRVwyxNbAkEAyh3sipmcgN7SD8xBG/MtBYPqWP1vxhSVYPfJzuPU3gS5MRJzQHBz" +
                 Environment.NewLine + "sVCLhTBY7hHSoqiqlqWYasi81JzBEwEuQQJBAKw9qGcZjyMH8JU5TDSGllr3jybx" +
                 Environment.NewLine + "FFMPj8TgJs346AB8ozqLL/ThvWPpxHttJbH8QAdNuyWdg6dIfVAa95h7Y+MCQEZg" +
                 Environment.NewLine + "jRDl1Bz7eWGO2c0Fq9OTz3IVLWpnmGwfW+HyaxizxFhV+FOj1GUVir9hylV7V0DU" +
                 Environment.NewLine + "QjIajyv/oeDWhFQ9wQECQCydhJ6NaNQOCZh+6QTrH3TC5MeBA1Yeipoe7+BhsLNr" +
                 Environment.NewLine + "cFG8s9sTxRnltcZl1dXaBSemvpNvBizn0Kzi8G3ZAgc=" +
                 Environment.NewLine + "-----END RSA PRIVATE KEY-----";
            try
            {
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider
                {
                    PersistKeyInCsp = true
                };

                rsa.LoadPrivateKeyPEM(sPrivateKeyPEM);

                ASCIIEncoding encoder = new ASCIIEncoding();
                byte[] binData = encoder.GetBytes(signatureString);

                SHA256CryptoServiceProvider sha256 = new SHA256CryptoServiceProvider();
                byte[] signature = rsa.SignData(binData, sha256);

                return Convert.ToBase64String(signature);
            }
            catch(Exception ex)
            {
                return "";
            }
            
        }        

    }
}