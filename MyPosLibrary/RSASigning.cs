using System;
using System.Security.Cryptography;
using System.Text;

namespace MyPosLibrary
{ 
    public class RSASigning
    {        
        public string CreateSignaturePublic(string signatureString,int idClienteIcnea)
        {
            string sPublicKeyPEM = "";

            switch (idClienteIcnea)
            {
                case 8:
                    break;

                case 1952:                    
                    sPublicKeyPEM = "-----BEGIN CERTIFICATE-----" +
                         Environment.NewLine + "MIICDTCCAXagAwIBAgIENU7a8DANBgkqhkiG9w0BAQsFADAdMQswCQYDVQQGEwJC" +
                         Environment.NewLine + "RzEOMAwGA1UEChMFbXlQT1MwHhcNMTkwODI5MTMxMDMxWhcNMjkwODI2MTMxMDMx" +
                         Environment.NewLine + "WjAdMQswCQYDVQQGEwJCRzEOMAwGA1UEChMFbXlQT1MwgZ8wDQYJKoZIhvcNAQEB" +
                         Environment.NewLine + "BQADgY0AMIGJAoGBAMJa+MsMEif2CEPXLS2B28QtxGja+GvcPWhaFmsGjIEEDeAt" +
                         Environment.NewLine + "13cgWXRnLyt6LCRf6zeviwinXB0UZ9vxR1xPT4RW+KjZVgGDWsVkhGxou7VcL715" +
                         Environment.NewLine + "F5EoR0WAVW1ZAkMOnwI0aBr1qDEZ+sjlbSRy8qa3QyBhOxMESX5FUroVeWRTAgMB" +
                         Environment.NewLine + "AAGjWjBYMB0GA1UdDgQWBBRK+L9tCOp7xWkvaXiz/MK/6xSy1jAfBgNVHSMEGDAW" +
                         Environment.NewLine + "gBRK+L9tCOp7xWkvaXiz/MK/6xSy1jAJBgNVHRMEAjAAMAsGA1UdDwQEAwIE8DAN" +
                         Environment.NewLine + "BgkqhkiG9w0BAQsFAAOBgQA0FgJRpeFZhPX452ckRbouiC/xBJQuHibzHZTLSMHt" +
                         Environment.NewLine + "di3NEi6qC/FjhNDzjFGS2AEK2abpTVX6go0J7ubhuG7KuMqHu9oia9bW06/9zeed" +
                         Environment.NewLine + "eM2znYkKo/NAxWxfKNStS3yQVjQVu+y4RBVTdlUHYx2HN+oWOj+50/V0FgnNKik/" +
                         Environment.NewLine + "FA==" +                         
                         Environment.NewLine + "-----END CERTIFICATE-----";
                    break;

                case 1902:
                    sPublicKeyPEM = "-----BEGIN CERTIFICATE-----" +
                         Environment.NewLine + "MIICDDCCAXWgAwIBAgIDfvCYMA0GCSqGSIb3DQEBCwUAMB0xCzAJBgNVBAYTAkJH" +
                         Environment.NewLine + "MQ4wDAYDVQQKEwVteVBPUzAeFw0xOTEwMjkxMjUyMDJaFw0yOTEwMjYxMjUyMDJa" +
                         Environment.NewLine + "MB0xCzAJBgNVBAYTAkJHMQ4wDAYDVQQKEwVteVBPUzCBnzANBgkqhkiG9w0BAQEF" +
                         Environment.NewLine + "AAOBjQAwgYkCgYEAx5/jJKhna084AMnqQeY4r2R1FIsniFbeTQzx4jxmOVLytvCS" +
                         Environment.NewLine + "5QHlda5Tkius6yFr9VyTj8ZOCxep6ZA/o7+vWKOScNSty7Uru6OcBgQ1x0FKNNJA" +
                         Environment.NewLine + "y46ovxcbp5TGex5qW7jPCUhOr+FAiHg1Ch80SK65ICNKVY7FLdgXhPnh+esCAwEA" +
                         Environment.NewLine + "AaNaMFgwHQYDVR0OBBYEFBcgnGdOmXi/xRhyqfuoFYgE7Y2jMB8GA1UdIwQYMBaA" +
                         Environment.NewLine + "FBcgnGdOmXi/xRhyqfuoFYgE7Y2jMAkGA1UdEwQCMAAwCwYDVR0PBAQDAgTwMA0G" +
                         Environment.NewLine + "CSqGSIb3DQEBCwUAA4GBAAabn+P1sXABn4jRnsMwI2QVFVxrvVvdWiveXp+cekyZ" +
                         Environment.NewLine + "O2YaY4ugT9whoMRrhdbju1yqDh3PGiumksSjjpAP0SFn/d7bCANO0xUgM2SIVxDp" +
                         Environment.NewLine + "uf54T4O+mhydU3/3vjZ+2KNoCQ35gXfuKHbxjqIhVXx7SzYYLKBfrgiLLL4zfI3e" +                         
                         Environment.NewLine + "-----END CERTIFICATE-----";
                    break;

                case 1918:
                    break;

                default:
                    break;
            }            
           
            try
            {
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider
                {
                    PersistKeyInCsp = true
                };

                rsa.LoadPublicKeyPEM(sPublicKeyPEM);

                ASCIIEncoding encoder = new ASCIIEncoding();
                byte[] binData = encoder.GetBytes(signatureString);

                SHA256CryptoServiceProvider sha256 = new SHA256CryptoServiceProvider();
                byte[] signature = rsa.SignData(binData, sha256);

                return Convert.ToBase64String(signature);
            }
            catch(Exception ex)
            {
                return "";
            }
            
        }

        public string CreateSignature(string signatureString, int idClienteIcnea)
        {
            string sPrivateKeyPEM = "";

            switch (idClienteIcnea)
            {
                case 8:
                    // Prurba cambios 2019-11-13.
                    // Clave privada ICNEA necesaria para generar clave de acceso.
                    sPrivateKeyPEM = "-----BEGIN RSA PRIVATE KEY-----" +
                         Environment.NewLine + "MIICWwIBAAKBgQC8ihCF10ZLTMwrOALIKbz7y4DOiykhJ5Y5RhW2tQUxnnoSA9f+" +
                         Environment.NewLine + "8T16PSNxwAwDR/PLk1Xp1BM4Ysu5L5B9u+C/90ROBGFS0kdYAzVxxApG4D/f3+1i" +
                         Environment.NewLine + "2nr3TuiJTfRUCscrUHGdqB37mXguTNECsnaCnVLs8mgBGgnXs8/0r8s+6wIDAQAB" +
                         Environment.NewLine + "AoGAOLbHVZt2N7x4Rh/pJlD32j5TFs5hpumDlOLyMS2uzEhBrB2f4b6roZO2j8cX" +
                         Environment.NewLine + "Vf1U5s4BL/9tcK63XZNjQY88gb2Il+q0r33LGH/NVV2T1Q+jbAYbHz4WCTAtDLIE" +
                         Environment.NewLine + "MwptukgSL6gEfbUfDFWTkw7jGEE9arlrAzYsIKw5/MJzRYECQQDywCB1g7tpm3OJ" +
                         Environment.NewLine + "QYmwHTCPG+aGo31WYPlJOBNB234pyF99bx52nMQY45N/sa1f05ttkzYYKZxJbEph" +
                         Environment.NewLine + "0wxz9w3hAkEAxtR2cWFXfLAR8MzN8FRrRrg/w2s57u5lTsULjPs9hcYXZ1OH+BSB" +
                         Environment.NewLine + "Dut8br5ZDoLq3r+Vumf95p8tYJnq2ePuSwJANXqKLBl3Yk6y51we30Ou/PgikyrV" +
                         Environment.NewLine + "ZAA9KxpFc87NPps8HkAqr+rdcLZCOnGERoYxybQSyMPyAGLxCBgcuDeHQQJAQt6H" +
                         Environment.NewLine + "qyAaxVON5RdsF2y8237x6u4wALxqk/BPLy38BfaWPmjobLWwpezq32/jEa9UAHAc" +
                         Environment.NewLine + "kiGRUbOtFjUNuBQhOwJAOvssl4wSgQiECdxbSExWT0OugtFLzsMO7ODtdugz1eLU" +
                         Environment.NewLine + "yyKf9Z79CtMIb0rV+uc7jQaDBSpFdQBJA7Lk87OUvw==" +
                         Environment.NewLine + "-----END RSA PRIVATE KEY-----";

                    break;

                case 1952:
                    // Clave privada cedida por el cliente 1952 necesaria para generar clave de acceso.
                    sPrivateKeyPEM = "-----BEGIN RSA PRIVATE KEY-----" +
                         Environment.NewLine + "MIICXQIBAAKBgQDONvshIOVbIw0oeMvgm1w+NzOFS/IOycOn7dPa5YFACSdogsR8" +
                         Environment.NewLine + "OJL981UISwwHuONi9wyS6mvdfMGLJ54ci6qhXPgyCiLdd9BsEfCCbeHSLvufc4Xn" +
                         Environment.NewLine + "Orw3hnMS+aZNec07erR5QqeMejpB1rO/SAY5sGFstobMvHqvdtThq9y5uQIDAQAB" +
                         Environment.NewLine + "AoGBAIv+74aSGqNArJIpT9/wGTNzrlp0MKSxfU7oOseEv7aFgGeHP3VFwyT8qWGz" +
                         Environment.NewLine + "8Trc3iI1riefVhNLuumJv+GJQBCLigt07uUccLwDMWVKqDKowpj00VPk012ITooB" +
                         Environment.NewLine + "FNqAUgL99g+BIyvCfPmoHIeewst9CaEYe5e/2fle1HucN7gBAkEA90Fg0tT66GW8" +
                         Environment.NewLine + "xx65KKAFHlt026qqURTwb6W+KEEOTyEO3pXamxWyPk845C1PQxtw9lGeHNUIdBwT" +
                         Environment.NewLine + "Gs3mQolUwQJBANWCBpg1XKM47EKp94IT/iya6LQbK6lr+f1M9ha/VNFBP7CTAAOj" +
                         Environment.NewLine + "i8+mSYWyJTNHeiFZKvLryYKoWFJH9qSpyvkCQQCdY2NzsRjqF+C7y0XHo1y+vy66" +
                         Environment.NewLine + "qX9yz2ZhW1qZGxg1rHBfpXoa4Auu+BTZwjHDINOeJ8QCzGRuLHTIyin5TpvBAkBk" +
                         Environment.NewLine + "+D3II4wwwI2ypRCg2nWcgJHGAwdt8tthd9D1RvyEjTfieFJMZj6LL9ebO3kuPAI8" +
                         Environment.NewLine + "3R9bjDha6uQB3Xh9vLJ5AkAik36OiUeL3A9rrHfLaxpw8QDcj7iDIRKeEWGhg1bl" +
                         Environment.NewLine + "b0qO84/qwVD1SodaZPU1Sc2soZdLE5ayhyNIoFvj3b/i" +
                         Environment.NewLine + "-----END RSA PRIVATE KEY-----";
                    break;

                case 1918:
                    // Clave privada cedida por el cliente 1918 novecentocase necesaria para generar clave de acceso.
                    sPrivateKeyPEM = "-----BEGIN RSA PRIVATE KEY-----" +
                         Environment.NewLine + "MIICWwIBAAKBgQChpxW8Ym2Q/W3Qy7sTxkt0s3BPl14iSfoyktbaCstbXjoRxPNQ" +
                         Environment.NewLine + "4Bk2JpppBwdqlxjiNFuQ6YPyaN/TtWQQD+DTzztoNh2C+CdrZ0VPH5Hiz7hXMkwB" +
                         Environment.NewLine + "4s//FO5+32fyWQZ8Fe5u0kmc2nxY+mkBoa14i/87hXMsH4Hr3uFLgsnQOwIDAQAB" +
                         Environment.NewLine + "AoGAV1Y+mBlVuRAzVKu912E7Q0HvXBEb+sqdMtXC3usWE7dFCHSwQPHSrT8ODdCO" +
                         Environment.NewLine + "YThhaVWAYzrG3yFqTKej+Emb3fgRhcGQGgru2l8FfjyTvq3DW365zfr5c8UAAi1Y" +
                         Environment.NewLine + "lrNA6Vo6XRNvMYZKVR51nn+mNIJ/5yvHlAWTgQLoiU522IECQQDTCTCSDhBCQzlM" +
                         Environment.NewLine + "V+4UXxX/W4VziNL4fxHQ7Xfyvbl9LOrstgNo3RaRNy/ssYzH/+ZtsPl+9xVriTv9" +
                         Environment.NewLine + "CnFvE92dAkEAxBhSG/ptE/2bY7Dl8fdCVX7gxL1OsVkIk8vDJ7MZp70iX+kCnyBf" +
                         Environment.NewLine + "B/Dc313xKxUwpx+RpPm3+0qoFdOGRiVptwJAUG6GNoRpqfeZFQHGWblL1wF4wWn6" +
                         Environment.NewLine + "jn9PuQNo3fl1gsHl38YXUpZBDaOs/ldFQszDdDcpQDzHyy6I0I50YVN++QJAad7T" +
                         Environment.NewLine + "neVjtNJMbdNZIn0jZDNfOPkYHmn6VolBcRKiPEzB3VRZJP5+MDRbm4EFrwP+w4wo" +
                         Environment.NewLine + "vxBPw613+Gi+aurSGwJAF/CEbsoKxmsyWgYb29RW/Z31402OdBQxs8c0gxRxZFdc" +
                         Environment.NewLine + "nvxfyrV1kYtTAYDCWw4A4bunxOpTyeCYHXg+B56jiw==" +
                         Environment.NewLine + "-----END RSA PRIVATE KEY-----";
                    break;

                case 1902:
                    // Clave privada cedida por el cliente 1902 naturadrada necesaria para generar clave de acceso.
                    sPrivateKeyPEM = "-----BEGIN RSA PRIVATE KEY-----" +
                         Environment.NewLine + "MIICXQIBAAKBgQCvZJEW40JeHAeQOuBA0RjsruP2G/P8v2GZVCyPGn3RidShSi72" +
                         Environment.NewLine + "ui5k+c7gX/08pteogAChOChdx4E+xgyKTJOgYTkQJFpPLDepAtONi/QHmt2xKOp9" +
                         Environment.NewLine + "dQOKGESp4isRlfnYIyNEI8HMkjazVBewJ0WHHiN2NtrDgyjjZhLuFcFXCwIDAQAB" +
                         Environment.NewLine + "AoGBAKrylgd80T7wit6kw9Xo9yYY/87sR85cqj/LrvJ7m44TjM73gvavWHDKmjxO" +
                         Environment.NewLine + "0n+/DgsJV5PLLGza+rm4Rpi1MlxMsSVkyQQJdbyKme828vfUN1UkcDK2ndLMHnn1" +
                         Environment.NewLine + "2pcKZWzd/wjvYMOLDeNpTiO1mhm8O+sLVgHyhKntMgUAogvBAkEA5Eo4i5zg3pMG" +
                         Environment.NewLine + "Gmu/NeDC7ueChqtovcUMoGPE3s3fLscSCXOeGbsLroeRO6r0Vg8weCcCY22buJEn" +
                         Environment.NewLine + "IIbp3fXlIwJBAMSupTw/Q1y4opX62gTzCOEPVFb5p19oY78lwXR2Amb0vJBtt5Vg" +
                         Environment.NewLine + "qPSr2lwR0USQhA4V0QR2EEWOd7s/SHOYKPkCQQDS1Mzl+ysdwzB2xB1h98JverVs" +
                         Environment.NewLine + "A++Gmu3vTlSp4/2DKOw6eGGBp+HA/IifT2G5/Ospe/VTuA2BZeR5wL9Jcud7AkA0" +
                         Environment.NewLine + "TxaXHWQ3BswHA350joawBLmzoBZJo6PTtKDs7m3ZIAH+j4+h5OFKoWIj1oFHsP/O" +
                         Environment.NewLine + "FzNN8nCFb0ycukn0pGVRAkAYBWeipPPPoPpWwbH/tWX/6ABK/T68x/4lahWzSTSx" +
                         Environment.NewLine + "SqKMw6V3nSjst3fKox5KJPhPLxja+fA40z+RBV+VVVo0" +
                         Environment.NewLine + "-----END RSA PRIVATE KEY-----";
                    break;

                default:
                    // Clave privada cedida por MyPOS necesaria para generar clave de acceso.
                    sPrivateKeyPEM = "-----BEGIN RSA PRIVATE KEY-----" +
                        Environment.NewLine + "MIICXAIBAAKBgQCf0TdcTuphb7X+Zwekt1XKEWZDczSGecfo6vQfqvraf5VPzcnJ" +
                        Environment.NewLine + "2Mc5J72HBm0u98EJHan+nle2WOZMVGItTa/2k1FRWwbt7iQ5dzDh5PEeZASg2UWe" +
                        Environment.NewLine + "hoR8L8MpNBqH6h7ZITwVTfRS4LsBvlEfT7Pzhm5YJKfM+CdzDM+L9WVEGwIDAQAB" +
                        Environment.NewLine + "AoGAYfKxwUtEbq8ulVrD3nnWhF+hk1k6KejdUq0dLYN29w8WjbCMKb9IaokmqWiQ" +
                        Environment.NewLine + "5iZGErYxh7G4BDP8AW/+M9HXM4oqm5SEkaxhbTlgks+E1s9dTpdFQvL76TvodqSy" +
                        Environment.NewLine + "l2E2BghVgLLgkdhRn9buaFzYta95JKfgyKGonNxsQA39PwECQQDKbG0Kp6KEkNgB" +
                        Environment.NewLine + "srCq3Cx2od5OfiPDG8g3RYZKx/O9dMy5CM160DwusVJpuywbpRhcWr3gkz0QgRMd" +
                        Environment.NewLine + "IRVwyxNbAkEAyh3sipmcgN7SD8xBG/MtBYPqWP1vxhSVYPfJzuPU3gS5MRJzQHBz" +
                        Environment.NewLine + "sVCLhTBY7hHSoqiqlqWYasi81JzBEwEuQQJBAKw9qGcZjyMH8JU5TDSGllr3jybx" +
                        Environment.NewLine + "FFMPj8TgJs346AB8ozqLL/ThvWPpxHttJbH8QAdNuyWdg6dIfVAa95h7Y+MCQEZg" +
                        Environment.NewLine + "jRDl1Bz7eWGO2c0Fq9OTz3IVLWpnmGwfW+HyaxizxFhV+FOj1GUVir9hylV7V0DU" +
                        Environment.NewLine + "QjIajyv/oeDWhFQ9wQECQCydhJ6NaNQOCZh+6QTrH3TC5MeBA1Yeipoe7+BhsLNr" +
                        Environment.NewLine + "cFG8s9sTxRnltcZl1dXaBSemvpNvBizn0Kzi8G3ZAgc=" +
                        Environment.NewLine + "-----END RSA PRIVATE KEY-----";
                    break;
            }

            try
            {
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider
                {
                    PersistKeyInCsp = true
                };

                rsa.LoadPrivateKeyPEM(sPrivateKeyPEM);

                ASCIIEncoding encoder = new ASCIIEncoding();
                byte[] binData = encoder.GetBytes(signatureString);

                SHA256CryptoServiceProvider sha256 = new SHA256CryptoServiceProvider();
                byte[] signature = rsa.SignData(binData, sha256);

                return Convert.ToBase64String(signature);
            }
            catch (Exception ex)
            {
                return "";
            }

        }

    }
}